data "aws_iam_policy" "codecommit" {
  arn = "arn:aws:iam::aws:policy/AWSCodeCommitPowerUser"
}

resource "aws_iam_user" "codecommit" {
  name = "wildrydes-site"

  force_destroy = true
}

resource "aws_iam_access_key" "codecommit_user" {
  user = aws_iam_user.codecommit.name
}

# resource "aws_iam_user_policy" "codecommit" {
#   name   = "codecommit-policy"
#   user   = aws_iam_user.codecommit.name
#   policy = data.aws_iam_policy.codecommit.
# }

resource "aws_iam_policy_attachment" "test-attach" {
  name       = "wildrydes-site-attch"
  users      = [aws_iam_user.codecommit.name]
  policy_arn = "arn:aws:iam::aws:policy/AWSCodeCommitPowerUser"
}

resource "aws_iam_service_specific_credential" "codecommit" {
  service_name = "codecommit.amazonaws.com"
  user_name    = aws_iam_user.codecommit.name
}

resource "aws_codecommit_repository" "app" {
  repository_name = "wildrydes-site"
  description     = "This is the wildrydes-site Repository"

  tags = merge(
    {
      Name = "${local.prefix}-codecommit"
    },
    local.common_tags
  )
}