locals {
  resource_name = local.prefix
}

resource "aws_iam_role_policy" "lambda_role_policy" {
  name = "amplify_iam_role_policy"
  role = aws_iam_role.iam_for_amplify.id

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AmplifyCodeCommitPull",
          "Effect" : "Allow",
          "Action" : [
            "codecommit:GitPull"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["amplify.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_amplify" {
  name               = "iam_for_amplify"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_amplify_app" "app" {
  name       = "wildrydes-site"
  repository = local.repository_name

  basic_auth_credentials = base64encode("${local.service_user_name}:${local.service_password}")
  iam_service_role_arn   = aws_iam_role.iam_for_amplify.arn

  # # The default build_spec added by the Amplify Console for React.
  build_spec = <<-EOT
  version: 1
  frontend:
    phases:
      # IMPORTANT - Please verify your build commands
      build:
        commands: []
    artifacts:
      # IMPORTANT - Please verify your build output directory
      baseDirectory: /
      files:
        - '**/*'
    cache:
      paths: []
  EOT

  enable_auto_branch_creation = true
  enable_branch_auto_build    = true
  enable_branch_auto_deletion = true
  platform                    = "WEB"

  auto_branch_creation_config {
    enable_pull_request_preview = true
    environment_variables = {
      APP_ENVIRONMENT = "develop"
    }
  }

  # The default rewrites and redirects added by the Amplify Console.
  custom_rule {
    source = "/<*>"
    status = "404"
    target = "/index.html"
  }

  # environment_variables = {
  #   ENV = "test"
  # }
}

resource "aws_amplify_branch" "develop" {
  app_id      = aws_amplify_app.app.id
  branch_name = "main"

  enable_auto_build = true

  # framework = "Next.js - SSR"
  # stage     = "DEVELOPMENT"

  environment_variables = {
    APP_ENVIRONMENT = "develop"
  }
}