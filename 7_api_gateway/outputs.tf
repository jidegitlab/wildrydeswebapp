output "invoke_url" {
  value = aws_api_gateway_stage.prod.invoke_url
}

output "source_arn" {
  value = "arn:aws:execute-api:${var.aws_region}:${var.account_id}:${aws_api_gateway_rest_api.WildRydesAPI.id}/*/${aws_api_gateway_method.post_method.http_method}${aws_api_gateway_resource.ride.path}"
}
