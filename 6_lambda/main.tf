locals {
  remote_state_bucket_region = "us-east-1"
  remote_state_bucket        = "wildrydes-site-remote-state-s3"
  dynamodb_state_file        = "wildrydes-site-dynamodb.tfstate"
  amplify_state_file         = "wildrydes-site-amplify.tfstate"

  dynamodb_arn           = data.terraform_remote_state.dynamodb.outputs.dynamodb_arn
  amplify_app_domain_url = data.terraform_remote_state.amplify.outputs.webapp_url_domain
}

data "terraform_remote_state" "dynamodb" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.dynamodb_state_file
  }
}

data "terraform_remote_state" "amplify" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.amplify_state_file
  }
}