resource "aws_dynamodb_table" "rides" {
  name           = "${local.prefix}-users"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "RideID"
  tags           = local.common_tags

  attribute {
    name = "RideID"
    type = "S"
  }
}

resource "aws_ssm_parameter" "rides_table_arn" {
  name      = "${local.ssm_prefix}/rides-table-arn"
  type      = "String"
  overwrite = true
  value     = aws_dynamodb_table.rides.arn
}
