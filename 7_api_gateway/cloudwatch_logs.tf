data "aws_iam_policy_document" "cloudwatch" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "cloudwatch" {
  name               = "WildRydesAPICloudWatch"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_api_gateway_account" "WildRydesAPI" {
  cloudwatch_role_arn = aws_iam_role.cloudwatch.arn
}


resource "aws_iam_role_policy" "cloudwatch" {
  name   = "WildRydesAPILogsIAMPolicy"
  role   = aws_iam_role.cloudwatch.id
  policy = data.aws_iam_policy_document.cloudwatch.json
}

resource "aws_api_gateway_method_settings" "wildcard_logs" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
  stage_name  = aws_api_gateway_stage.prod.stage_name
  method_path = "*/*"
  settings {
    metrics_enabled = true
    logging_level   = "INFO"
  }

  depends_on = [aws_api_gateway_account.WildRydesAPI]
}

# resource "aws_api_gateway_method_settings" "post_logs" {
#   rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
#   stage_name  = aws_api_gateway_stage.dev.stage_name
#   method_path = "ride/POST"

#   settings {
#     metrics_enabled = true
#     logging_level   = "INFO"
#   }

#   depends_on = [aws_api_gateway_account.WildRydesAPI]
# }