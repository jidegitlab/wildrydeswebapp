terraform {
  backend "s3" {
    bucket         = "wildrydes-site-remote-state-s3"
    key            = "wildrydes-site-amplify.tfstate"
    region         = "us-east-1"
    encrypt        = "true"
    dynamodb_table = "wildrydes-site-remote-state-dynamodb"
  }
}
