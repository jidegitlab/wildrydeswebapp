locals {
  remote_state_bucket_region = "us-east-1"
  remote_state_bucket        = "wildrydes-site-remote-state-s3"
  infrastructure_state_file  = "wildrydes-site-infrastructure.tfstate"
  codecommit_state_file      = "wildrydes-site-codecommit.tfstate"

  prefix            = data.terraform_remote_state.infrastructure.outputs.prefix
  common_tags       = data.terraform_remote_state.infrastructure.outputs.common_tags
  service_password  = data.terraform_remote_state.codecommit.outputs.service_password
  service_user_name = data.terraform_remote_state.codecommit.outputs.service_user_name
  repository_name   = data.terraform_remote_state.codecommit.outputs.repository_name
}

data "terraform_remote_state" "infrastructure" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.infrastructure_state_file
  }
}

data "terraform_remote_state" "codecommit" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.codecommit_state_file
  }
}
