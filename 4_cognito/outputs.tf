output "pool_id" {
  value = aws_cognito_user_pool.WildRydes.id
}

output "app_client_id" {
  value = aws_cognito_user_pool_client.WildRydesWebApp.id
}

output "user_pool_arn" {
  value = aws_cognito_user_pool.WildRydes.arn
}
