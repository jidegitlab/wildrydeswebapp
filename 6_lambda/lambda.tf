resource "aws_iam_role_policy" "lambda_role_policy" {
  name = "lambda_iam_role_policy"
  role = aws_iam_role.iam_for_lambda.id

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "LambdaDynamoDBPolicy",
          "Effect" : "Allow",
          "Action" : [
            "dynamodb:PutItem",
            "dynamodb:DescribeStream",
            "dynamodb:GetRecords",
            "dynamodb:GetShardIterator",
            "dynamodb:ListStreams"
          ],
          "Resource" : [
            "${local.dynamodb_arn}"
          ]
        },
        {
          "Sid" : "LambdaCloudWatchLogsPolicy",
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : [
            "${aws_cloudwatch_log_group.request_unicorn.arn}"
          ]
        }
      ]
    }
  )
}
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}
resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "archive_file" "lambda" {
  type        = "zip"
  source_file = "index.js"
  output_path = "${path.module}/lambda.zip"
}

resource "aws_lambda_function" "request_unicorn_lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.

  filename      = "lambda.zip"
  function_name = var.lambda_function_name
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "index.handler"

  source_code_hash = data.archive_file.lambda.output_base64sha256

  runtime = "nodejs16.x"

  # Make sure the role policy is attached before trying to use the role
  depends_on = [aws_iam_role_policy.lambda_role_policy, aws_cloudwatch_log_group.request_unicorn]
  environment {
    variables = {
      AMPLIFY_APP_URL = "https://main.${local.amplify_app_domain_url}"
    }
  }
}

resource "aws_cloudwatch_log_group" "request_unicorn" {
  name              = "/aws/lambda/${var.lambda_function_name}"
  retention_in_days = 14
  lifecycle {
    prevent_destroy = false
  }
}