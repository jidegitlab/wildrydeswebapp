variable "aws_region" {
  default     = "us-east-1"
  description = "AWS Region to deploy VPC"
}

variable "account_id" {
  default     = "212491648264"
  description = "AWS account id"
}
