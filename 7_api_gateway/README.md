![API Gateway](https://res.cloudinary.com/jidecloudy/image/upload/v1689074054/cloud/wildrydes-infra/d9twwacwjlruppmlsgdo.png)

![API Gateway Options method](https://res.cloudinary.com/jidecloudy/image/upload/v1689074053/cloud/wildrydes-infra/y3mfmsqyudoi1a0mkxwd.png)

![API Gateway Post method](https://res.cloudinary.com/jidecloudy/image/upload/v1689074052/cloud/wildrydes-infra/xgs5hx2mxtnzrxhz6gzc.png)

![API Gateway Cognito Authorizer](https://res.cloudinary.com/jidecloudy/image/upload/v1689074052/cloud/wildrydes-infra/ctwoxpgwyeqjz3uj1m34.png)

![API Gateway Metrics](https://res.cloudinary.com/jidecloudy/image/upload/v1689074048/cloud/wildrydes-infra/yydjtkbyi8wtes5punvj.png)

![API Gateway Execution Logs](https://res.cloudinary.com/jidecloudy/image/upload/v1689074046/cloud/wildrydes-infra/i25rxqpbd6zveud8y961.png)