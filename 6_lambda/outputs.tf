output "lambda_function_arn" {
  value = aws_lambda_function.request_unicorn_lambda.arn
}

output "lambda_function_name" {
  value = aws_lambda_function.request_unicorn_lambda.function_name
}

output "lambda_invoke_arn" {
  value = aws_lambda_function.request_unicorn_lambda.invoke_arn
}

