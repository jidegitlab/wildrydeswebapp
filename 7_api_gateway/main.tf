locals {
  remote_state_bucket_region = "us-east-1"
  remote_state_bucket        = "wildrydes-site-remote-state-s3"
  infrastructure_state_file  = "wildrydes-site-infrastructure.tfstate"
  lambda_state_file          = "wildrydes-site-lambda.tfstate"
  cognito_state_file         = "wildrydes-site-cognito.tfstate"
  amplify_state_file         = "wildrydes-site-amplify.tfstate"

  prefix      = data.terraform_remote_state.infrastructure.outputs.prefix
  common_tags = data.terraform_remote_state.infrastructure.outputs.common_tags

  amplify_app_domain_url = data.terraform_remote_state.amplify.outputs.webapp_url_domain

  lambda_function_arn  = data.terraform_remote_state.lambda.outputs.lambda_function_arn
  lambda_function_name = data.terraform_remote_state.lambda.outputs.lambda_function_name
  lambda_invoke_arn    = data.terraform_remote_state.lambda.outputs.lambda_invoke_arn

  cognito_user_pool_arn = data.terraform_remote_state.cognito.outputs.user_pool_arn
}

data "terraform_remote_state" "infrastructure" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.infrastructure_state_file
  }
}

data "terraform_remote_state" "amplify" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.amplify_state_file
  }
}

data "terraform_remote_state" "lambda" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.lambda_state_file
  }
}

data "terraform_remote_state" "cognito" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.cognito_state_file
  }
}