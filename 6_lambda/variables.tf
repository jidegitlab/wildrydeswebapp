variable "aws_region" {
  default     = "us-east-1"
  description = "AWS Region to deploy VPC"
}
variable "lambda_function_name" {
  default = "RequestUnicorn"
}
