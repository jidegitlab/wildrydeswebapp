output "service_password" {
  value     = aws_iam_service_specific_credential.codecommit.service_password
  sensitive = true
}

output "service_user_name" {
  value = aws_iam_service_specific_credential.codecommit.service_user_name
}

output "repository_name" {
  value = aws_codecommit_repository.app.clone_url_http
}
