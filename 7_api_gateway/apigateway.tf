resource "aws_api_gateway_rest_api" "WildRydesAPI" {
  name        = "WildRydes"
  description = "This is my API for WildRydes app"

  endpoint_configuration {
    types = ["EDGE"]
  }

  tags = merge(
    {
      Name = "${local.prefix}-apigateway"
    },
    local.common_tags
  )
}

resource "aws_api_gateway_resource" "ride" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
  parent_id   = aws_api_gateway_rest_api.WildRydesAPI.root_resource_id
  path_part   = "ride"
}

# Add a restricted HTTP-POST-request method to api resource created above
resource "aws_api_gateway_method" "post_method" {
  rest_api_id   = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id   = aws_api_gateway_resource.ride.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.api_authorizer.id
}
# Create a response for the post method created above
resource "aws_api_gateway_method_response" "post_method_response_200" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id = aws_api_gateway_resource.ride.id
  http_method = aws_api_gateway_method.post_method.http_method
  status_code = tonumber(200)

  depends_on = [aws_api_gateway_method.post_method]
}

# Integrate the POST method created above with the lambda function being created in lambda.tf
resource "aws_api_gateway_integration" "post_integration" {
  rest_api_id             = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id             = aws_api_gateway_resource.ride.id
  http_method             = aws_api_gateway_method.post_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/${local.lambda_function_arn}/invocations"

  request_templates = {
    "application/json" = jsonencode(
      {
        statusCode = 200
      }
    )
  }
  # depends_on = [aws_api_gateway_method.post_method]
}

# Create an 'integration response' (presumably the message returned from lambda function?!)
resource "aws_api_gateway_integration_response" "post_integration_response" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id = aws_api_gateway_resource.ride.id
  http_method = aws_api_gateway_method.post_method.http_method
  # status_code = aws_api_gateway_method_response.post_200.status_code
  status_code = tonumber(200)

  depends_on = [aws_api_gateway_integration.post_integration]
}

# Add an unrestricted HTTP-POST-request method to api resource created above
resource "aws_api_gateway_method" "options_method" {
  rest_api_id   = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id   = aws_api_gateway_resource.ride.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

# Create a response for the options method
resource "aws_api_gateway_method_response" "options_200" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id = aws_api_gateway_resource.ride.id
  http_method = aws_api_gateway_method.options_method.http_method
  status_code = tonumber(200)
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
  depends_on = [aws_api_gateway_method.options_method]
}

# Integrate options with a mock function
resource "aws_api_gateway_integration" "options_integration" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id = aws_api_gateway_resource.ride.id
  http_method = aws_api_gateway_method.options_method.http_method
  type        = "MOCK"

  request_templates = {
    "application/json" = jsonencode(
      {
        statusCode = 200
      }
    )
  }
  depends_on = [aws_api_gateway_method.options_method]
}

# Create response for options integration
resource "aws_api_gateway_integration_response" "options_integration_response" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id
  resource_id = aws_api_gateway_resource.ride.id
  http_method = aws_api_gateway_method.options_method.http_method
  status_code = tonumber(200)
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
    "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS,POST,PUT'",
    # still had to add this manually in AWS console
    "method.response.header.Access-Control-Allow-Origin"  = "'https://main.${local.amplify_app_domain_url}'"
    # "method.response.header.Access-Control-Allow-Origin"  = "'*'"
  }
  depends_on = [aws_api_gateway_method_response.options_200]
}

resource "aws_api_gateway_deployment" "WildRydes" {
  rest_api_id = aws_api_gateway_rest_api.WildRydesAPI.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.WildRydesAPI.body))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "prod" {
  deployment_id = aws_api_gateway_deployment.WildRydes.id
  rest_api_id   = aws_api_gateway_rest_api.WildRydesAPI.id
  stage_name    = "prod"
}

# Create permission for api resource to invoke lambda function at specified route
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = local.lambda_function_name
  principal     = "apigateway.amazonaws.com"
  # source_arn    = "${aws_api_gateway_rest_api.WildRydesAPI.arn}/*"
  source_arn = "arn:aws:execute-api:${var.aws_region}:${var.account_id}:${aws_api_gateway_rest_api.WildRydesAPI.id}/*/${aws_api_gateway_method.post_method.http_method}${aws_api_gateway_resource.ride.path}"
}