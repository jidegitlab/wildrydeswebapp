output "webapp_url_domain" {
  value = aws_amplify_app.app.default_domain
}

output "webapp_id" {
  value = aws_amplify_app.app.id
}
