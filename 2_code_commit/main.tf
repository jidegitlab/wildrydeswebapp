locals {
  remote_state_bucket_region = "us-east-1"
  remote_state_bucket        = "wildrydes-site-remote-state-s3"
  infrastructure_state_file  = "wildrydes-site-infrastructure.tfstate"

  prefix      = data.terraform_remote_state.infrastructure.outputs.prefix
  common_tags = data.terraform_remote_state.infrastructure.outputs.common_tags
}

data "terraform_remote_state" "infrastructure" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.infrastructure_state_file
  }
}
